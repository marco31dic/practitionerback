//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function(req,res,next){
  res.header("Acces-Control-Allow-Origin","*");
  res.header("Acces-Control-Allow-Headers","Origin,X-Requested-With,Content-Type,Accept");
  next();
});

var requestjson = require('request-json');

var path = require('path');

var urlMovimientos = "https://api.mlab.com/api/1/databases/mespinoza/collections/movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt"
var clienteMlab = requestjson.createClient(urlMovimientos);

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/",function(req,res){
  //res.send("Hola mundo node js");
  res.sendFile(path.join(__dirname, 'index.html'));
})

app.post("/",function(req,res){
  res.send("Hemos recibido su peticiòn post");
})

app.get("/clientes/:idcliente",function(req,res){
  //res.send("Aqui tienes al cliente nùmero: " + req.params.idcliente);
  var mijson = "{'idcliente':12345}";
  res.send(mijson);
})

app.put("/",function(req,res){
  res.send("Hemos recibido su peticiòn put cambiado");
})

app.delete("/",function(req,res){
  res.send("Hemos recibido su peticiòn delete");
})

app.get("/v1/movimientos",function(req,res){
  res.sendFile(path.join(__dirname, 'movimientosv1.json'));
})

var movimientosJSON = require('./movimientosv2.json');

app.get("/v2/movimientos",function(req,res){
  res.json(movimientosJSON);
})

app.get("/v2/movimientos/:id",function(req,res){
  console.log(req.params.id);
  res.send(movimientosJSON[req.params.id-1]);
})

app.get('/v2/movimientosq', function (req,res){
  console.log(req.query);
  res.send("recibido");
  //localhost:3000/v2/movimientosq?id=33&otrovalor=hola
})


var bodyparser = require('body-parser'); //parsear el json
app.use(bodyparser.json());
app.post('/v2/movimientos',function(req,res){
  var nuevo = req.body;
  nuevo.id = movimientosJSON.length + 1;
  movimientosJSON.push(nuevo);
  res.send("Movimiento dado de alta");
})

app.get('/movimientos',function(req,res){
  var clienteMlab = requestjson.createClient(urlMovimientos);
  clienteMlab.get('',function(err, resM, body){
    if (err){
      console.log(err);
    }else{
      res.send(body)
    }
  })
})

app.post('/movimientos',function(req,res){
  clienteMlab.post('',req.body,function(err, resM, body){
    res.send(body);
  });
})
